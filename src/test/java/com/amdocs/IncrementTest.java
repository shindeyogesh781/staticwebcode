package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

        @Test
        public void runDescrementCounter() throws Exception {
        int i = new Increment().decreasecounter(2);
                assertEquals("add", 1, i);
	
        int j = new Increment().decreasecounter(0);
                assertEquals("add", 1, j);
	
        int k = new Increment().decreasecounter(1);
                assertEquals("add", 1, k);
        }
}
