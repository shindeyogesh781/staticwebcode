package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {

	@Test
	public void runAdd() throws Exception {	
	int k = new Calculator().add(2,3);
		assertEquals("add", 5, k);

	}
	
	@Test
	public void runSub() throws Exception {
	int k= new Calculator().sub(9,6);
		assertEquals("sub",3,k);
	}

}
